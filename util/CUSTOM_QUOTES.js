// these should be straight from the API
var x = new Map();
x.set('Living in the moment means letting go of the past and not waiting for the future. It means living your life consciously, aware that each moment you breathe is a gift.', 'Oprah Winfrey');
module.exports = x;
