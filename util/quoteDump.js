var request = require('request');
var cheerio = require('cheerio');
var RSVP = require('rsvp');

function brainyQuoteDump (buffer) {
  var calls = [];

  for (var page = 0; page <= 9; ++page) {
    calls.push(function (p) {
      return new RSVP.Promise(function(resolve, reject) {
        request('https://www.brainyquote.com/quotes/topics/topic_motivational' + p + '.html', function (error, response, body) {
          if (!error && response.statusCode == 200) {
            var $ = cheerio.load(body);
            var quotes = $('div#quotesList div.bqQt');
            if (quotes.length <= 0) {
              reject('no quotes');
            }

            var results = [];
            for (var i=0; i < quotes.length; ++i) {
              var rawQuote = quotes[i];
              var quote = $('a[title="view quote"]', rawQuote).text();
              var author = $('a[title="view author"]', rawQuote).text();

              // Print the data inside of single quotes with a comma at the end so we can drop this into
              // an array. Also escape all single quotes
              // console.log('q: ' + "'" + quote.replace(/'/g, '\\\'') + "',");
              // console.log('a: ' + "'" + author.replace(/'/g, '\\\'') + "',");

              results.push({
                quote: quote,
                author: author
              });
            }

            resolve(results);
          } else {
            reject('call failed: ' + error);
          }
        });
      });
    }(page));
  }

  return RSVP.all(calls);
}

function quotationsPageDump (buffer) {
  var calls = [];
  var MILLI_PER_DAY = 86400000;
  var yesterday = new Date((new Date()).getTime() - MILLI_PER_DAY);
  // First quotes are on http://www.quotationspage.com/mqotd/2004-01-01.html
  // var current = new Date(Date.parse("01/01/2014"));
  var current = new Date(Date.parse("01/01/2016"));
  for (; current < yesterday; current = new Date(current.getTime() + MILLI_PER_DAY)) {
    var pageDate = current.toISOString().substring(0, 10); // trim "2014-01-01T08:00:00.000Z" to "2014-01-01"
    calls.push(function (p) {
      return new RSVP.Promise(function(resolve, reject) {
        request('http://www.quotationspage.com/mqotd/' + p + '.html', function (error, response, body) {
          if (!error && response.statusCode == 200) {
            var $ = cheerio.load(body);
            var quotes = $('dt.quote a[href *= "/quote/"]');
            var authors = $('dd.author a[href *= "/quotes/"]');
            if (quotes.length <= 0) {
              throw('no quotes');
            }

            var results = [];
            for (var i = 0; i < quotes.length; ++i) {
              // Print the data inside of single quotes with a comma at the end so we can drop this into
              // an array. Also escape all single quotes
              var rawQuote = quotes[i];
              if (rawQuote != null && rawQuote.children[0] != null) {
                var quote = rawQuote.children[0].data.trim();
                var rawAuthor = authors[i];
                var author = (rawAuthor != null && rawAuthor.children[0] != null)? rawAuthor.children[0].data.trim() : 'Unknown'
                // console.log('q: ' + "'" + quote.replace(/'/g, '\\\'') + "'," + 'DESMONDV' + 'a: ' + "'" + author.replace(/'/g, '\\\'') + "',");
                results.push({
                  quote: quote,
                  author: author
                });
              }
            }

            resolve(results);
          } else {
            reject('call failed: ' + error);
          }
        });
      });
    }(pageDate));
  }

  return RSVP.all(calls);
}

function quoteliciousDump () {
  var calls = [];
  
  for (var page = 1; page <= 9; ++page) {
    var pageParam = (page == 1 ? '' : '/page/' + page);
    calls.push(function (p) {
      return new RSVP.Promise(function(resolve, reject) {
        request('http://quotelicious.com/quotes/motivational-quotes' + p, function (error, response, body) {
          if (!error && response.statusCode == 200) {
            var $ = cheerio.load(body);
            var quotes = $('div#content-quotespage div.post');
            if (quotes.length <= 0) {
              throw('no quotes');
            }

            var results = [];
            for (var i = 0; i < quotes.length; ++ i) {
              var rawQuote = quotes[i];
              var quote = $('a', rawQuote).text();
              var author = $('em', rawQuote).text().substring(2).trim();
              // // Print the data inside of single quotes with a comma at the end so we can drop this into
              // // an array. Also escape all single quotes
              // console.log('q: ' + "'" + quote.replace(/'/g, '\\\'') + "',");
              // console.log('a: ' + "'" + author.replace(/'/g, '\\\'') + "',");

              results.push({
                quote: quote,
                author: author
              });
            }

            resolve(results);
          } else {
            reject('call failed: ' + error);
          }
        });
      });
    }(pageParam));
  }

  return RSVP.all(calls);
}

// Log the value in the format for the source code:
// '<value>',
// value will have ' and " escaped. Junk characters will be removed
function emitValue (value) {
  var cleanValue = value
    .replace(/'/g,'\\\'')
    .replace(/"/g, '\\\"')
    .replace(/�/g, ' ')
    .replace(/�/g, '\\\'')
    .replace(/�/g, '\\\'');
  console.log('\'' + cleanValue + '\',');
}

var BLACKLIST = require("./QUOTE_BLACKLIST");
var CUSTOM_QUOTES = require("./CUSTOM_QUOTES");

var quotes = {
  a: brainyQuoteDump(),
  b: quotationsPageDump(),
  c: quoteliciousDump()
};

RSVP.hash(quotes).then(function (results) {
  var addedQuotes = new Set();
  var buffer = [];
  function processResult (res) {
    for (var i=0; i < res.length; ++i) {
      for (var j=0; j < res[i].length; ++j) {
        if (!BLACKLIST.has(res[i][j].quote) && !addedQuotes.has(res[i][j].quote)) {
          addedQuotes.add(res[i][j].quote);
          buffer.push(res[i][j]);
        }
      }
    }
  }

  // Process the custom quotes list
  CUSTOM_QUOTES.forEach(function (author, quote, map) {
    if (!BLACKLIST.has(quote) && !addedQuotes.has(quote)) {
      addedQuotes.add(quote);
      buffer.push({
        quote: quote,
        author: author
      });
    }
  });

  // Process the API results
  processResult(results.a);
  processResult(results.b);
  processResult(results.c);

  // Emit the quotes
  for (var i=0; i < buffer.length; ++i) {
    emitValue(buffer[i].quote);
  }

  // Emit the authors
  for (var i=0; i < buffer.length; ++i) {
    emitValue(buffer[i].author);
  }
});
