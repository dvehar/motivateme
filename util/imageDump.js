function imageDump () {
    var request = require('request');
    var cheerio = require('cheerio');
    request('https://api.flickr.com/services/rest/?&method=flickr.favorites.getList&api_key=' + process.env.FLICKR_KEY + '&user_id=48889646@N07&per_page=500', function (error, response, body) {
        if (!error && response.statusCode == 200) {
            var $ = cheerio.load(body);
            var photos = $('photo');
            for (var i=0; i < photos.length; ++i) {
            	var photo = photos[i];
            	console.log('[' + photo.attribs.farm + ', ' + photo.attribs.server + ', ' + photo.attribs.id + ', \'' + photo.attribs.secret + '\'],');
            }
        } else {
            throw('call failed: ' + error);
        }
    });
}

imageDump();
