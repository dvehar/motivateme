// these should be straight from the API
var x = new Set();

x.add('Be Impeccable With Your Word. Speak with integrity. Say only what you mean. Avoid using the word to speak against yourself or to gossip about others. Use the power of your word in the direction of truth and love.');
x.add('If you believe in yourself enough and know what you want, you’re gonna make it happen.');
x.add('When yu\' can\'t have why you choose, yu\' just choose what you have.');

module.exports = x;
